#!/usr/bin/env bash
ansible-playbook ./local.yml --ask-vault-pass && eval $(ssh-agent -s) && ssh-add ~/.ssh/grish_p_ed25519 && git clone git@gitlab.com:CountGrish/dotfiles.git ~/.dotfiles && sh .dotfiles/ansible/.config/ansibleScripts/run.sh
